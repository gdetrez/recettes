---
title: Tome blanche
categories: Fromage
---

## Ingrédients

- Trois litres de lait entier de préférence cru
- une cuillère a café de yaourt
- une cuillère a café de présure

## Préparation

1.  Je monte mon lait à 34 degrés, j'ai préalablement ajouté le yaourt
2.  Je verse la présure, brasse un peu et je vais laisser cailler cinquante
    minutes
3.  Avec un couteau je tranche mon caillé en cube de 2 cm par 2 cm
4.  J'attends de voir un peu de sérum remonter et je brasse légèrement
5.  Je découpe mon caillé en cube de 1 cm par 1 cm
6.  Je verse le caillé dans deux moules
7.  Pour faciliter l'égouttage je n'hésite pas à secouer doucement mes moules
8.  Je vais attendre une petite heure
9.  Je rassemble les deux moules ensemble pour ne faire qu'un fromage
10.  Je laisse égoutter cinq heures et je mets le tout au frigo dans une
     assiette ! La première étape est faite, vous pouvez lancer vos invitations
     pour votre repas d'alpage ;)

## Sources

https://lalaiteriedeparis.blogspot.com/2015/07/je-fais-de-la-tome-blanche-comme-en.html
http://pagecouleur.over-blog.com/article-fabrication-de-tomme-blanche-124897200.html
