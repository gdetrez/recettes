---
title: "Chaussons Champignons Maroilles"
date: 2023-12-21T14:47:49+01:00
categories: Tartes
source: https://www.notparisienne.fr/entrees/chaussons-aux-champignons-et-maroilles/
---

Pour 4 personnes (en entrée, 2 en plat principal).

## Ingrédients

- 1 pâte feuilletée
- 120 g de maroilles
- 100 g de champignons
- 1 oignon
- 1 gousse d'ail
- 2 c. à s. d'huile d'olive
- 2 c. à c. de crème fraiche
- 2 c. à c. de moutarde

Pour la dorure

- 1 c. à s. d'huile d'olive
- 1 pincée de curcuma
- 1 pincée de paprika

## Instructions

Émincez l'oignon finement et la gousse d'ail. Coupez les champignons en
tranches fines.

Faites revenir l'oignon à feu très doux à couvert 10 minutes dans une casserole
avec l'huile, ajoutez un peu d'eau à mi-cuisson si nécessaire.

Réservez, et dans la même poêle mettez les champignons, l'ail et faites cuire 5
à 10 minutes à couvert.

Étalez la pâte .

Coupez-la en deux. Etalez dessus la moutarde puis la crème fraîche puis les
oignons, les champignons, le fromage.

Refermez et soudez bien les bords avec une fourchette.

Réalisez la dorure en mélangeant l'huile, le paprika, le curcuma et appliquez
ce mélange dessus à l'aide d'un pinceau.

Enfournez pour 30 minutes à 180°C.
