---
title: "Mexican Garlic Soup with Oregano & Fresh Lime"
source: "https://portandfin.com/mexican-garlic-soup-with-oregano-fresh-lime/"
categories: "Soupes"
---

## Ingredients

- 4 Tbsp olive oil
- 15 cloves garlic, crushed
- 8 cups chicken or vegetable stock- ideally homemade
- 1½ tsp salt (omit if using store bought, pre-salted stock)
- 3 eggs, lightly beaten
- 2 Tbsp fresh oregano, finely chopped
- 2 limes, 1 juiced, 1 cut into wedges
- ½ baguette

## Instructions

1. Preheat oven to 325 degrees.
2. In a large soup pot, heat olive oil over low heat. Add garlic and cook until garlic is soft but not coloured and oil is well flavoured. Remove from heat.
3. Slice baguette into cubes or ¼ inch thick slices. In a bowl, toss bread with half of the garlic infused olive oil and some salt and pepper. Spread on a baking sheet and bake for 10 minutes, until lightly toasted and crisp.
4. Pour stock into the soup pot with the remaining oil and garlic. Simmer on medium heat. Add 1 tsp salt and chopped oregano. Gradually add eggs, stirring constantly. Simmer and stir until the eggs set, roughly 3-5 minutes. Remove from heat and add the juice of 1 lime.
5. Serve soup topped with garlic toasted bread and a side of lime wedges.
