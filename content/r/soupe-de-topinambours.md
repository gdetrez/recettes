---
title: Soupe de topinambours simple
categories: Soupes
---

## Ingrédient

* 750 g topinambours
* 250 g pommes de terre
* 2 oignons
* 1 l d'eau
* 2 bouillons cubes
* 3dl crème

## Notes

Fait à l'instant pot, 20 minutes. Trop liquide: peut-être que ½ L d'eau suffirait ?
Essayer de cuire 15 min seulement ?

Source: papier qui traine dans la cuisine à Grevegårdsvägen.
