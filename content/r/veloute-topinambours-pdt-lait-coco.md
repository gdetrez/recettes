---
title: "Velouté de topinambours, au lait de coco et gingembre"
date: 2022-05-23T19:48:03+02:00
categories: Soupes
source: http://gourmandiz.hautetfort.com/archive/2013/01/17/veloute-de-topinambour-pommes-de-terres-au-lait-de-coco-et-g.html
---


# Ingrédients

- 500 g de topinambours
- 400 g de pommes de terre à chair farineuse
- 1 oignon
- 400 ml de lait de coco
- 200 ml d'eau
- 1 cc grönsaksfond
- 4 bonne pincées de gingembre en poudre
- 2 pincées de curcuma en poudre
- 2 pincées de curry
- sel

## Réalisation

1. Couper les pommes de terre, les topinambours et l'oignon en morceaux
2. Faire revenir 2 minutes les légumes dans un peu de beurre (ou ghee) dans une cocotte
3. Ajouter le lait de coco, l'eau, le fond, les épices et le sel
4. Faire cuire 5 minutes à haute pression
5. Mixer jusqu'à obtention d'une texture bien veloutée et servir
