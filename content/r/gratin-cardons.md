---
title: Gratin de cardons
categories: Roasts
yield: 4 pers.
source: https://www.laurentmariotte.com/ma-recette-de-gratin-de-cardons/
---

Préparation : 45 min
Cuisson : 20 min

## Ingrédients (pour 4 pers.)

- 1 kg de jeunes cardons
- 3 citrons
- 30 g de farine
- 30 g de beurre
- ½ litre de lait
- 10 cl de fond de veau
- 1 grosse pincée de noix de muscade
- 1 cuil. à soupe de crème épaisse
- Sel, poivre du moulin

## La recette

1.  Coupez les feuilles (non comestibles) du cardon, coupez-le et lavez-le.
    Ôtez les parties filandreuses et tranchez les cardons en tronçons
    réguliers. Plongez-les immédiatement dans une casserole remplie eau
    citronnée et salée pour ne pas qu’ils noircissent.
2.  Portez cette casserole à ébullition et laissez cuire entre 20 et 30 min.
3.  Dans une casserole faites fondre la farine et le beurre et laissez cuire 1
    min. Ajoutez petit à petit le lait en remuant. Laissez chauffer jusqu’à ce
    que la sauce commence à s’épaissir. Ajoutez le fond de veau, la crème
    épaisse, la noix de muscade et laissez mijoter pendant 5 min. Salez et
    poivrez.
4.  Préchauffez le four à 180°C (th.6). Beurrez un plat à gratin, ajoutez les
    cardons mélangés à la sauce et enfournez pendant 20 min jusqu’à ce que le
    dessus soit bien doré.
