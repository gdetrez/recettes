---
title: "Velouté de topinambours"
date: 2022-05-23T19:58:27+02:00
categories: Soupes
source: https://danslacuisinedesophie.fr/2020/01/veloute-de-topinambours.html
---


# Ingrédients

- 1 kg de topinambours
- 1 oignon
- 2 pommes de terre
- 1 quantité suffisante d'huile de tournesol
- 15 cl de crème liquide ou crème végétale
- sel
- poivre
- quelques pluches de persil plat

# Préparation

- Peler les topinambours et les pommes de terre puis les détailler en cubes.
- Peler et émincer l'oignon.
- Faire chauffer un peu d'huile dans l'instant pot, y faire revenir sur feu doux l'oignon pendant quelques minutes. Saler.
- Ajouter les cubes de topinambours et les pommes de terre.
- Les faire suer quelques minutes en mélangeant régulièrement, saler.
- Ajouter 500 ml d'eau, et mettre à cuire pour 5 minutes. Les légumes doivent être tendres.
- À la fin de la cuisson, relacher la pression manuellement.
- Mixer la soupe.
- Verser la crème et bien mélanger.
- Rectifier l'assaisonnement en sel et poivrer.
