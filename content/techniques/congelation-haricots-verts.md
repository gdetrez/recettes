---
title: "Congelation haricots verts"
date: 2022-09-22T21:58:52+02:00
categories: Conserves
source: https://www.papillesetpupilles.fr/2021/07/comment-congeler-les-haricots-verts-frais.html/
---

* Laver
* Blanchir 1:30 dans une grande casserole d'eau bouillante sallée
* Choquer dans un bain de glace
* Égouter et sécher
* Équeuter (le fait de le faire a posteriori évite aux haricots verts de se gorger d’eau)
* Mettre sous vide
* Congeler


