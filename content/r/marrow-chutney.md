---
title: Marrow chutney
categories: Condiments
yield: about 4 x 450 g jars
source: https://www.bbcgoodfood.com/recipes/marrow-chutney
---

Prep:40 mins
Cook:25 mins
Plus 12 hrs salting


## Ingredients

- 1.5kg/3lb 5 oz marrow , peeled and deseeded
- 225g shallot , sliced
- 225g apple , peeled, cored and sliced
- 225g sultana
- 2cm piece ginger , finely chopped
- 225g demerara sugar
- 850ml malt vinegar
- 12 black peppercorns


## Method

Cut the marrow into small pieces, put in a bowl and sprinkle liberally with 2
tbsp salt. Cover and leave for 12 hrs.

Rinse and drain the marrow, then place in a preserving pan or large saucepan
with the shallots, apples, sultanas, ginger, sugar and vinegar. Tie the
peppercorns in muslin (or put into a small enclosed tea strainer) and place in
the pan. Bring to the boil, then reduce the heat and simmer, stirring from time
to time, until the consistency is thick.

Leave to settle for 10 mins, then spoon into sterilised jars (see tip below),
put on the lids and label. Will keep for a year in a cool, dark place.
