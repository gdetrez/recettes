---
title: "Bon Appétit's Chocolate Buckwheat Waffles"
date: 2024-03-31T18:49:39+02:00
categories: Dessert
---

## Ingredients

8 servings

### Toppings

- ¼ cup mixed buckwheat groats, black or white sesame seeds, and/or flaxseeds
- 2 teaspoons plus 2 tablespoons pure maple syrup
- Pinch of kosher salt
- 1 cup whole-milk ricotta

### Waffles and Assembly

- 1 cup (125 g) buckwheat flour
- ½ cup Dutch-process unsweetened cocoa powder
- ¼ cup (30 g) flaxseed meal
- 1¼ teaspoons kosher salt
- 1 teaspoon baking powder
- 1 teaspoon baking soda
- 2 large eggs, room temperature
- 2 cups buttermilk (filmjölk)
- ½ cup virgin coconut oil, melted
- ¼ cup (packed) dark brown sugar
- 2 teaspoons vanilla extract
- 2 ounces bittersweet chocolate, coarsely chopped
- Nonstick vegetable oil spray
- Pure maple syrup (for serving)

## Preparation

### Toppings

1. Preheat oven to 300°. Toss buckwheat, 2 tsp. maple syrup, and salt on a
   rimmed baking sheet until evenly coated. Bake until dry to the touch and
   toasty smelling, 12–15 minutes. Stir to recoat and transfer to a sheet of
   parchment paper. Let crumble cool.
2. Whisk ricotta and remaining 2 Tbsp. maple syrup in a small bowl until
   smooth.

### Waffles and Assembly

1. Heat a waffle iron on medium. Whisk buckwheat flour, cocoa powder, flaxseed
   meal, salt, baking powder, and baking soda in a large bowl to combine. Whisk
   eggs, buttermilk, coconut oil, brown sugar, and vanilla in a medium bowl
   until smooth. Add to dry ingredients and whisk until smooth; mix in
   chocolate.
2. Lightly coat waffle iron with nonstick spray. Scoop batter onto waffle iron
   (it should cover the entire surface; amount needed will vary according to
   model) and cook waffles until you smell a blast of chocolaty aroma hit you
   and edges are slightly darkened, around 3 minutes per batch. Remove
   carefully from waffle iron. Without gluten, waffles will be extra tender.
3. Serve waffles topped with whipped ricotta, crumble, and maple syrup.

## Sources

- https://www.bonappetit.com/recipe/gluten-free-chocolate-and-buckwheat-waffles
- https://www.youtube.com/watch?v=Z7mqj5stFMg
