---
title: Tangy Cilantro-Lime Quinoa Salad
categories: Salades
source: https://ohsheglows.com/2013/07/19/cumin-lime-black-bean-quinoa-salad-quick-easy/
---

- yield: 4 servings
- prepTime: 20 minutes
- cookTime: 15 minutes

This black bean quinoa salad is so fresh and tangy! It's delicious paired with
creamy avocado and fresh pita bread or crackers. If you're not as big of a fan
as I am of tangy/acidic dressings, feel free to reduce the amount of lime juice
called for.

## Ingredients

For the salad:

- 1 cup uncooked quinoa (or 3 cups cooked)
- 1 (14-ounce/398 mL) can black beans (or 1 1/2 cups cooked), drained and rinsed
- 1 1/2 cups fresh cilantro, finely chopped
- 3 medium carrots, julienned (about 1 1/2 cups) or 1 large chopped and roasted sweet potato
- 4 green onions, thinly sliced
- Fine grain sea salt and black pepper, to taste
- [Lemon-Tahini Dressing](3-lemon-tahini-dressing.md) (optional)
- Sliced avocado (optional)
- Endurance Crackers, pita bread, or crackers of choice (optional)

For the dressing:

- 5 to 5 1/2 tablespoons (75 to 82.5 mL) fresh lime juice, to taste
- 4 tablespoons (60 mL) extra-virgin olive oil
- 2 large garlic cloves, minced
- 2 teaspoons ground cumin
- 2 teaspoons (10 mL) pure maple syrup, or to taste
- 3/4 teaspoon fine grain sea salt, or to taste

### Directions

To prepare the quinoa: Rinse quinoa in a fine mesh sieve. Add into pot
along with 1.5 cups (375 mL) water or veggie broth. Bring to a boil, reduce
heat to low-medium, and then cover with a tight-fitting lid. Simmer for 14
to 17 minutes until the water is absorbed and the quinoa is fluffy. Remove
from heat and steam with the lid on for 5 additional minutes. Fluff with
fork and chill in the fridge for at least 15 minutes.

In a large bowl, toss the quinoa, drained and rinsed black beans, cilantro,
carrots (or roasted sweet potato), and green onions.

Whisk together the dressing in a small bowl or jar. Adjust to taste if desired.
Pour onto salad and toss to combine. Season with salt and pepper to taste until
everything pops.

Spoon into bowl and drizzle on some Lemon-Tahini Dressing. Top with sliced
avocado and serve with Endurance Crackers, if desired. 

Tips: The flavours tend to mellow when this salad sits in the fridge, as the
quinoa absorbs the dressing a lot. I recommend giving it a "refresh" by adding
a squeeze of lime juice, a drizzle of olive oil, and more seasonings to taste.
Stir it all together, and taste.
