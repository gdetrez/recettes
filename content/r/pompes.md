---
title: Pompes avec la farine du moulin
categories: Crêpes
source: https://lalaiteriedeparis.blogspot.com/2015/06/que-faire-du-petit-lait-des-pompes-bien.html
yield: ~15 pompes
---

## Ingrédients

- 225 grammes de farine de sarrasin
- 225 grammes de farine de froment ( ça doit pouvoir se faire en pur sarrasin mais la je suis la recette de Patricia à la lettre ! )
- 0.5 sachet de levure de boulanger
- 1 litre de petit lait que j'ai récupéré lors de ma fabrication de tome fraîche

## Préparation

Je mélange tout, je laisse reposer une heure, et je fais mes pompes, assez
épaisses…

Elles se gardent super bien sous un torchon ! Agrémentées de saint nectaire et
de jambon de pays, c'est le repas du dimanche soir :) Bon appétit !

## Notes

Fait avec la farine du moulin.  La pâte avait l'air un peu seche, essayer avec
plus de liquide / moins de farine.

2022.04.16 Fait avec 50 de farine en moins.  Le résultat est moins sec.
Crêpière, gros feu sur 4, ~2:30 de chaque côté.

## Fourrage aux champinions

Béchamel (50 g beurre, 50 g farine blanche, 500 ml lait, muscade) + 2 boites de
champinions à la poele et 1/2 paquet de feta. (12 crêpes)
