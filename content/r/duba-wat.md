---
title: "Duba Wat—Etiopisk och eritreansk pumpagryta"
date: 2024-04-27T16:17:48+02:00
categories: Ragoût
---

## Ingredienser
4 portioner
- 1 kg pumpa av valfri sort (butternut eller klassisk “hallowenpumpa” funkar bra)
- 2 gula eller röda lökar
- 3 vitlöksklyftor
- 1-3 msk berberikrydda (justera mängd efter smak)
- Salt
- Olja till stekning
- Ca 4 dl vatten
Serveringsförslag: Lättgjord injera eller ris

## Gör så här

Hacka lök i små tärningar, skala och skär pumpan i bitar, finhacka vitlöken.

Hetta upp en helt torr kastrull eller stekpanna med höga kanter. Stek löken
tills den börjar svettas och få färg, det tar ca 8 min. Tillsätt sedan olja och
fortsätt stek löken tills den får färg. Viktigt att ta sig tiden att steka
löken för godast smak på grytan.

Vänd i vitlök och berberikrydda, ju mer krydda du har i desto mer smak och
hetta får du i grytan, justera efter smak. Stek kryddan med löken i ca 4-5 min.
Tillsätt lite vatten, ca 1 dl och fräs allting.

Vänd pumpan och fräs allting nån minut. Tillsätt vatten och salt. Låt grytan
puttra under lock tills pumpan mjuknar, ca 20 min.

Servera med önskat tillbehör.

# Sources

- https://www.youtube.com/watch?v=nl8OeKFf9iQ
- https://zeinaskitchen.se/duba-wat/
