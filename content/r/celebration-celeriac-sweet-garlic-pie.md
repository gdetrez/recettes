---
title: Celebration celeriac and sweet garlic pie
source: https://www.bbc.co.uk/food/recipes/celebration_celeriac_and_33665
categories: Tartes
---

Preparation time: 30 mins to 1 hour
Cooking time: 1 to 2 hours
Serves: Serves 8–10

## Ingredients

For the pastry

- 250g/9oz plain spelt flour, plus extra for dusting
- 125g/4½oz cold unsalted butter, cubed
- ½ tsp fine sea salt
- few sprigs each fresh rosemary, thyme and sage, leaves picked and finely chopped
- 1 unwaxed lemon, finely grated zest only
- 25g/1oz Cheddar, grated
- 1 free-range egg yolk

For the filling

- 3 heads garlic, cloves separated and peeled
- olive oil
- 1 tsp balsamic vinegar
- 1 tbsp runny honey
- 2 sprigs each fresh rosemary, thyme and sage, leaves picked and finely chopped, plus extra to garnish
- 1kg/2lb 4oz celeriac, peeled, 300g/10½oz reserved, the rest cut into 2cm/¾in pieces
- 220g/8oz Lancashire cheese or cheddar, crumbled
- 150g/5½oz crème fraîche
- ½ lemon, juice only
- 1 tbsp wholegrain mustard
- small bunch fresh parsley, chopped
- dash vegetarian Worcestershire sauce
- 2 free-range eggs, beaten
- salt and freshly ground black pepper

## Method

To make the pastry, put the flour in a mixing bowl. Add the butter and salt and
rub gently with your fingertips until the mixture resembles fine breadcrumbs.
Stir through the herbs, lemon zest and cheddar.

Beat the egg yolk with 1 tablespoon of cold water in a bowl. Add to the flour
mixture and combine until it forms a dough, adding more cold water if needed.
Wrap in cling film and place in the fridge while you make the filling.

To make the filling, put the garlic in a saucepan, cover with cold water and
bring to a gentle simmer. Cook for 2–3 minutes, then drain.

Wipe the saucepan dry. Add the garlic and 1 tablespoon of olive oil and fry
over a high heat for 2 minutes. Add the vinegar and 100ml/3½fl oz water, bring
to the boil and simmer gently for 10 minutes.

Add the honey, most of the rosemary and thyme (reserving the rest, with the
sage) and a good pinch of salt. Cook over a medium heat for 5 minutes, or until
most of the liquid has evaporated and the garlic is coated in a dark syrup.

Meanwhile, put the celeriac in a saucepan, cover with freshly boiled water and
boil for 7–10 minutes, or until soft and slightly translucent. Drain and tip
into a big mixing bowl.

Add the cheeses, crème fraiche, lemon juice, mustard, parsley, Worcestershire
sauce and eggs. Add a good pinch of salt and grind of pepper and gently fold in
the garlic.

Preheat the oven to 200C/180C Fan/Gas 6.

Sprinkle flour onto a clean work surface and roll out the pastry to 3-4mm
thick. Line a 20cm/8in round cake tin with the pastry, ensuring a little spills
over the edges. Pour the filling into the pastry case. Coarsely grate the
reserved celeriac and pile it on top. Finish with the reserved rosemary, thyme,
sage and a drizzle of olive oil.

Bake for 45 minutes, or until the tart filling has set and the top is golden
brown. Leave to cool a little, then take it out of the tin. Serve warm with a
few herbs sprinkled over. 
