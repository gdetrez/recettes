---
title: "Vegansk Pannbiff Med Loksas"
date: 2024-02-10T19:18:01+01:00
categories: Husmanskost
---

## Ingredienser
Biffar

- 400 g Formbar färs från Anamma
- 1 st Gul lök
- 2 tsk Kryddpeppar
- 2 tsk Ljus sojasås
- 1 msk Mörk grönsaksfond
- 2 tsk Dijonsenap
- 1 msk Rapsolja
- Salt och rikligt med svartpeppar efter smak

Löksås
- 3 st Stora gula lökar
- 3 dl Havregrädde
- 1 msk Ljus sojasås
- 1.5 msk Mörk grönsaks- eller svampfond
- 2 tsk Torkad timjan
- 2 tsk Dijonsenap
- 1 tsk Socker
- Efter smak Rapsolja att steka i
- Efter smak Salt och svartpeppar efter smak

Tillbehör
- Pressgurka
- Valfri sorts potatis
- Rårörda lingon

## Gör så här
### Biffar

Tina färsen till kylskåpstemperatur.

Finhacka löken.

Blanda alla ingredienser till en jämn smet och smaka av med salt, peppar och
ev. lite mer kryddor.

Forma 4 stycken stora jämntjocka biffar.

Stek på medlevärme i rapsolja eller vegansmör tills de får fin färg på bägge
sidor, ca 4-5 min per sida. Låt dra på eftervärme ett tag så de reagerar hela
vägen igenom och känns fasta när du trycker på dem i mitten.

Ta ur pannan och lägg på ett fat medan du gör löksåsen.

När såsen är klar lägger du ner biffarna i pannan med såsen och serverar med
kokt potatis, pressgurka och rårörda lingon!

### Löksås

Börja med att strimla löken tunt och sedan steka på medelvärme i rapsolja tills
löken fått en fin mörkbrun färg och mjuknat helt.

Ta upp ur stekpannan och stek biffarna i samma panna.

När biffarna är färdigstekta så lägger du tillbaks den stekta löken, häller på
grädde och alla övriga kryddor till såsen. Låt koka upp och koka under
försiktig omrörning i 2-3 minuter tills smakerna gått ihop och såsen börjar
tjockna.

## Source

- https://javligtgott.se/recept/vegansk-pannbiff-med-loksas/
