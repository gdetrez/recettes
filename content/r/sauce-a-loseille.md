---
title: "Sauce à l'oseille"
date: 2024-04-20T13:55:09+02:00
categories: Sauces
---

# Ingrédients

- 1 dL crème
- 30 cl eau
- 15 cl vin blanc
- 1 échalotte
- 12 g oseille sans les tiges (~25 g avec)
- ½ bouillon cube
