---
title: Cheesy chard gratin
source: https://www.bbcgoodfood.com/recipes/cheesy-chard-gratin
categories: Roasts
---

Prep:15 mins
Cook:35 mins
Serves 4

## Ingredients

- bunch chard , about 340g
- 150ml double cream
- 1 tbsp wholegrain mustard (or gluten-free alternative)
- 140g gruyère , coarsely grated
- butter , for greasing
- 2 tbsp finely grated parmesan (or vegetarian alternative)

## Method

Heat oven to 200C/180C fan/ gas 6. Strip the chard leaves from the stalks, then
cut the stalks into sticks. Bring a pan of water to the boil and cook the
stalks for 3-4 mins until starting to soften. Then throw in the leaves for a
few moments too so that they just wilt. Drain well.

Mix the cream with the mustard, then toss through the chard with most of the
gruyère. Grease a medium gratin dish, spread the chard mix over, then scatter
with the remaining gruyère and the parmesan. Bake for 30 mins until bubbling
and golden. Serve straight from the dish.
