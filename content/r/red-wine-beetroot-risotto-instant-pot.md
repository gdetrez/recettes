---
title: "Instant Pot Red Wine & Beet Risotto"
date: 2023-08-12T11:50:09+02:00
categories: Onepot
tags: ["Instant Pot"]
source: https://ministryofcurry.com/red-wine-beetroot-risotto-instant-pot/#recipe
---

## Ingredients

- 1 tablespoon extra virgin olive oil
- 10 garlic cloves thinly sliced
- 1 small yellow onion finely diced
- 1 cup arborio rice
- ½ cup red wine
- 2 cups low sodium vegetable broth
- 2 small beetroots peeled and quartered
- ½ teaspoon kosher salt

Garnish

- 2 tablespoons Parmigiano Reggiano grated
- 2 tablespoons scallions thinly sliced
- ½ teaspoon black pepper freshly ground

# Instructions

Turn Instant Pot to Saute mode and heat oil. Add garlic slices and cook them
until they turn golden and crispy stirring frequently. Take the garlic out and
reserve, leaving the extra oil in the pot.

Add onions and cook for two minutes until they start to turn translucent and
soft. Add rice and lightly toast for a minute stirring often. Add red wine and
mix well, cooking for a minute as the wine reduces.

Add broth, beets, and salt.

Close the Instant Pot lid with the pressure valve to sealing. Pressure Cook
(Hi) for 7 minutes followed by natural pressure release.

Open the Instant Pot and take out the beets using a pair of tongs. Blend to a
smooth puree. Add the puree back to the Instant Pot and mix well. Add fresh
ground pepper.

Garnish with Parmigiano Reggiano, scallions, and reserved garlic slices. 
