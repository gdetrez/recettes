---
title: "Tofu Vietnamese Spring Rolls"
date: 2024-10-19T11:25:47+02:00
categories: Dim sum
---
## Ingredients

### Spring rolls

- 200 g julienned vegetables (such as carrots, red pepper, and cucumber)
- 60 g fresh cilantro
- 30 g fresh mint
- 113 g vermicelli or rice noodles (the thinner the better)
- 8-10 whole spring roll rice papers

### Almond butter dipping sauce

- 100 g salted creamy almond butter
- 15 ml reduced sodium soy sauce (tamari if gluten-free)
- 12.5-25 g brown sugar, agave, or honey if not vegan (depending on preferred sweetness)
- 15 ml fresh lime juice
- 1/2 tsp chili garlic sauce
- Hot water (to thin)

### Crispy tofu

- 227 g extra-firm tofu (drained and thoroughly dried/pressed)
- 60 ml sesame oil (divided)
- 21 g cornstarch
- 40 g almond butter dipping sauce
- 15 ml reduced sodium soy sauce (tamari if gluten-free)
- 14 g brown sugar or agave nectar

## Instructions

Start by preparing rice noodles in boiling hot water for about 10 minutes (read
instructions on package), then drain and set aside.

Meanwhile, heat a large skillet over medium heat and cut pressed tofu into
small rectangles. Toss in 3 Tbsp cornstarch and flash fry in ~3 Tbsp sesame oil
(amounts as original recipe is written // adjust if altering batch size),
flipping on all sides to ensure even browning – about 5 minutes. Remove from
skillet and set aside.

Prep veggies and prepare almond butter sauce by adding all sauce ingredients
except water to a small mixing bowl and whisk to combine. Add enough hot water
to thin until a pourable sauce is achieved. Adjust flavors as needed (I often
add a little more chili garlic sauce and brown sugar).

To add more flavor to the tofu, transfer ~2.5 Tbsp of the sauce to a small bowl
and add an additional Tablespoon each of soy sauce, sesame oil and brown sugar
or agave (amounts as original recipe is written // adjust if altering batch
size) and whisk to combine.

Add tofu back to the skillet over medium heat and add “sauce/glaze,” stirring
to coat. Cook for several minutes or until all of the sauce is absorbed and the
tofu looks glazed, stirring frequently (see photos). Set aside with prepared
veggies and vermicelli noodles.

To assemble spring rolls, pour very hot water into a shallow dish or skillet
and immerse rice paper to soften for about 10-15 seconds.

Transfer to a damp cutting board or damp towel and gently spread out edges into
a circle. It may take a little practice, so don’t feel bad if your first few
attempts are a fail!

To the bottom third of the wrapper add a small handful of vermicelli noodles
and layer carrots, bell peppers, cucumber, fresh herbs and 2-3 pieces of tofu
on top (see photo). Gently fold over once, tuck in edges, and continue rolling
until seam is sealed.

Place seam-side down on a serving platter and cover with damp warm towel to
keep fresh. Repeat until all fillings are used up – about 8-10 spring rolls
total (amount as original recipe is written // adjust if altering batch size).

Serve with almond butter sauce and sriracha or hot sauce of choice. I like to
mix mine and go dip happy.

Leftovers store well individually wrapped in plastic wrap, though best when
fresh.

## Sources

[Minimalist Baker](https://minimalistbaker.com/vietnamese-spring-rolls-with-crispy-tofu/#wprm-recipe-container-35869)
