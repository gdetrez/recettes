---
title: "Rabarberchutney"
date: 2022-06-30T20:58:28+02:00
categories: Condiments
source: https://www.expressen.se/alltommat/recept/rabarberchutney/
---

Makes: 1 liter

## Ingredienser

- 1 kg rabarber
- 2 gula lökar
- 50 g ingefära
- 1 chili - röd, t ex spansk peppar
- 2 dl rårössocker
- 1 dl vitvinsvinäger
- 1 tsk spiskummin - hel
- 1 tsk koriander - hel

##  Gör så här

Ansa och skiva rabarbern. Skala och strimla löken. Skala och finhacka
ingefäran. Kärna ur och  hacka chilin.

Blanda alla ingredienser i en gryta och låt koka ihop ca 20 min till en chutney.

Fyll chutneyn i väl rengjorda glasburkar. Förvara i kylen.
