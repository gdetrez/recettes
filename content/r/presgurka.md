---
title: Pressgurka
categories: Condiments
source: https://zeinaskitchen.se/pressgurka/
---

## Ingredienser
- 1 gurka- skivad tunt med mandolin eller osthyvel
- 0,5 tsk salt
- 2 msk ättika (12% styrka)
- 0,5 dl vatten
- 2 msk vitvinäger
- 2 msk socker
- 3/4 tsk vit- eller svartpeppar
- 1 dl finhackad persilja

## Gör såhär

Strö salt på gurkan och lägg en tyngd över. Låt stå i 30 min. Blanda ättika,
vinäger, socker, peppar och vatten. Rör om tills sockret löses upp. Häll av
gurkspadet som runnit av gurkan. Blanda ättiksvattnet och persiljan med gurkan
och ställ
