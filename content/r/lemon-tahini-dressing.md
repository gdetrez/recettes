---
title: Lemon-Tahini Dressing
categories: Sauces
source: https://ohsheglows.com/2011/05/12/lightened-up-protein-power-goddess-bowl/
---

## Ingredients

- 2 large garlic clove
- 1 cup fresh lemon juice
- 1/2 cup tahini
- 6 to 8 tablespoons nutritional yeast, to taste
- 6 to 8 tablespoons extra-virgin olive oil, to taste
- 1 teaspoon fine sea salt

## Directions

Mince the garlic in a mini food processor. Add the rest of the ingredients and
process until smooth. Set aside.
