---
title: Korean Sweet Soy-Glazed Potatoes (Gamja Bokkeum)
categories: Accompagnements
yield: 2 to 4 servings
source: https://www.seriouseats.com/korean-soy-glazed-potatoes-gamja-bokkeum-recipe
---

| Active time | 10 mins         |
| Total time  | 20 mins         |
| Serves      | 2 to 4 servings |

## Ingredients

- 3 tablespoons (45ml) soy sauce (see note)
- 3 tablespoons (45ml) water
- 2 tablespoons (30g) sugar
- 1 teaspoon (5ml) fish sauce
- 1 medium clove garlic (5g), finely grated
- 2 tablespoons (30ml) vegetable oil
- 10 ounces (285g) small Yukon Gold potatoes, rinsed and halved (see note)
- 1 tablespoon (15ml) toasted sesame oil
- 1 tablespoon (6g) toasted sesame seeds

## Directions

1.  In a small bowl, whisk together soy sauce, water, sugar, fish sauce, and
    garlic until sugar is dissolved, about 30 seconds. Set aside.
2.  Pour oil into a 3-quart saucier or saucepan. Add potatoes, arranging them
    cut-side down in a single layer. Set saucier over medium-high heat and
    cook, uncovered, until potatoes are light golden-brown on cut-side, 5 to 6
    minutes.
3.  Lower heat to medium-low, add soy sauce mixture and stir to combine with
    potatoes. Cover saucier with a lid, and cook until potatoes are completely
    tender and offer no resistance when pierced with a cake tester or paring
    knife, about 10 minutes.
4.  Remove lid and increase heat to high. Cook, uncovered, stirring constantly
    with a heat-resistant rubber spatula, until liquid has reduced and
    thickened to a sticky caramel consistency that fully glazes the potatoes, 1
    to 2 minutes.
5.  Remove from heat, and transfer potatoes to serving bowl. Drizzle with
    sesame oil, sprinkle with sesame seeds, and serve.
