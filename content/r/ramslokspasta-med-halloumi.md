---
title: Ramslökspasta med halloumi
yield: 2 personer
categories: Pasta
source: https://www.landleyskok.se/recept/ramslokspasta-med-halloumi
---

## Ingredienser

- ½ dl mandel
- ½ röd lök
- 200 g halloumi (1 paket)
- 20 blad ramslök
- ½ dl kallpressad olivolja
- 1 dl crème fraiche
- 200 g spaghetti

## Steps

Hacka mandeln och ha ner i en torr panna. Ha i en liten klick smör som får
steka in. Salta ett varv och rosta tills mandeln fårr färg. Skrapa av i en
skål.

Halvera löken och skär av ändarna. Skär sedan till smala klyftor mellan rot och
fäste. Värm upp stekpannan till medelvärme och ha ner löken i lite smör. Låt
den bara mjukna och ha sedan ner i en skål.

Vik ut halloumin-ostarna och skär av i viket så de blir till två plattor
vardera. Stek plattorna på båda sidor i stekpannan så de får yta. Tärna
halloumin och blanda med rödlöken.

Skölj ramslöksbladen om de har skrot på sig och torka dem. Ta bunten med
ramslöksblad och skär den slarvigt några gånger så bladen blir lite kortare. Ha
ner i en mixer tillsammans med oljan. Mixa till en grön olja och blanda den
sedan med crème fraiche. Rör ihop med lök och halloumi.

Koka pastan enligt anvisningen på förpackningen. Slå av vattnet och blanda
genast pastan med resten av ingredienserna. Lägg upp pastan i skålar och toppa
med den rostade mandeln.
