---
title: Matefaim
categories: Crêpes
source: https://cuisine.pagawa.com/recette-matefaim-matafan-savoyard-sale-5369
---

## Notes

Pour un repas léger à deux, faire la moitié?

## Ingrédients

Pour 2 matefaims

- 125 g de farine
- 50 g de fromage râpé
- 22.5 g de beurre (+ pour la cuisson)
- 20 cl de lait entier
- 2 oeufs (environ 200 g)
- 1/2 g de noix de muscade (en poudre ou râpée)
- Sel, poivre

## Préparation

1.  Faites fondre le beurre, puis fouettez-le dans un saladier avec les oeufs,
    le lait et la noix de muscade. Incorporez ensuite un peu de sel et de
    poivre, puis ajoutez la farine en remuant bien, jusqu'à ce qu'à obtenir une
    préparation homogène de matefaim.
2.  Avant chaque de cuisson de matafan, placez environ 10 g de beurre à fondre,
    puis versez une dose de pâte en inclinant la poêle pour bien l'étaler.
    Laissez cuire sur feu doux.
3.  Une fois que le dessus de la pâte ne coule plus, retournez votre matefaim
    savoyard avec une grande spatule, parsemez-le de fromage râpé et laissez
    cuire doucement l'autre face.
4.  Dégustez chaud, avec par exemple de la salade et/ou de la charcuterie en
    accompagnement.
