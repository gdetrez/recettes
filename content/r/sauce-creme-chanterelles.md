---
title: "Sauce à la crème et chanterelles"
date: 2022-05-23T19:53:47+02:00
categories: Sauces
source: https://www.bettybossi.ch/fr/Rezept/ShowRezept/BB_STPL090401_0034B-40-fr
---

Sers: 2

# Ingrédients

- 1 échalotte, finement haché
- 0.5 c.s. d'huile d'olive
- 100 g de chanterelles séchées
- 2 dl de crème entière
- 3 c.s. de vin blanc
- sel
- poivre

# Préparation


La veille, mettre les champignons à tremper dans la crème liquide.

Faire suer l’oignon dans l’huile chaude, ajouter les chanterelles, étuver un
court instant. Mouiller avec la crème et le vin, porter à ébullition, laisser
cuire doucement env. 10 min sur feu moyen, saler et poivrer.
