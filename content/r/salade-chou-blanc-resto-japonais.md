---
title: Salade de chou blanc comme au resto japonais
source: https://www.750g.com/comment-faire-une-salade-de-chou-blanc-comme-au-resto-japonais-r200173.htm
categories: Salades
---
## Ingrédients (4 personnes)

Pour la salade

- Chou blanc½ chou blanc
- Graine de sésameGrain de graine de sésame

Pour la marinade

- Vinaigre de riz ou vinaigre japonais15 cl de vinaigre de riz ou vinaigre japonais
- Sucre roux3 c. à s. de sucre roux

Pour la sauce

- Vinaigre de riz ou vinaigre japonais3 c. à s. de vinaigre de riz ou vinaigre japonais
- Huile d'olive3 c. à s. d'huile d'olive
- Mirin2 c. à s. de mirin
- Nuoc-mâm1 c. à s. de nuoc-mâm
- Sauce soja1 c. à c. de sauce soja


## Préparation

Préparation : 15min

Préparation du chou: Retirez la première couche de feuilles, celles qui sont un
peu vertes. Coupez le chou en deux puis retirez la base un peu dure. Vous
pouvez émincer votre demi-chou à l'aide d'un couteau ou le râper avec une râpe
suisse. Si vous avez une grande quantité de chou à râper, n'hésitez pas à
sortir un robot avec la lame pour émincer.

Préparation de la marinade: Dans un bol, mélangez les 15 cl de vinaigre de riz
avec les 3 cuillères à soupe de sucre roux, jusqu'à ce que le sucre soit bien
dissous. Faites mariner le chou dans cette marinade puis égouttez-le, rincez-le
et épongez-le sur du papier absorbant.

Assemblage: Versez le chou dans un grand bol, ajoutez la moitié de la sauce et
mélangez. Gardez le reste de la sauce dans un pot de confiture au frigo. Servez
et ajoutez les graines de sésame. Vous pouvez préparer cette salade à l'avance
et la garder au frais. En revanche, ajoutez toujours les graines de sésame à la
fin.

