---
title: Instant Pot Kale & Quinoa Pilaf
categories: Onepot
source: https://instantpoteats.com/instant-pot-kale-quinoa-pilaf/#tasty-recipes-12957-jump-target
---
## Ingredients (4)

- 1.5 cup dried quinoa
- 3–4 oz. / 100-120 g kale leaves (stems removed and torn into smaller pieces)
- 1/2 large onion, diced
- 2 tablespoons olive oil
- 1 small carrot, diced into small cubes
- 1–2 tablespoons raisins, roughly diced
- 1 garlic clove, diced
- 1 teaspoon lemon zest
- 1 vegetable stock cube (or 1.5 teaspoons powder, see notes)
- 1/2 teaspoon cumin powder (or paprika)
- 1+2/3 cup water

To finish
- 1/3 cup crumbled feta cheese (we used goat’s feta, omit for vegan version)
- 1/4 cup chopped fresh parsley
- 1/2 cup toasted almonds (see notes)
- Juice of 1/3 lemon (optional)

## Instructions

Rinse the quinoa under cold water and leave the sieve to strain. Prepare the
kale and set aside.

Tun the Instant Pot and turn the Sautee function key. Add the olive oil, onions
and carrots and cook for 2 minutes, stirring a couple of times.

Once you have sauteed the onions and carrots, add the quinoa, garlic, lemon
zest, raisins, vegetable stock cube (crush it with your hands) and cumin powder
(you can also use other spices or herbs). Add the water and stir through well.
Cancel the Saute function.

Note: A typical ratio of quinoa to water is 1:1 but we’re adding just a little
bit more water as we have other ingredients. This will also prevent that pesky
burning error message. 

Add the kale pieces on top of the quinoa in the broth, no need to stir through
at this stage. Pop the lid on top, close it, making sure the top valve is
pointing to Sealing. Press Manual/Pressure Cook button and adjust the time to 2
minutes. The Instant Pot will take 4-5 minutes to build up the pressure before
the time will begin. 

Once 2 minutes are up, allow the pressure to release naturally for 10 minutes.
Then, let off the rest of the steam manually by using the quick release method
(simply point the top valve to Venting). 

Open the lid and stir the quinoa with the kale. Transfer to a platter or a
large bowl and top with chopped parsley, feta cheese and toasted almonds. We
drizzled it with a little lemon juice as well, but that’s optional. Serve with
a salad and enjoy! 
