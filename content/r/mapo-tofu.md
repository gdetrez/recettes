---
title: Mapo tofu
categories: Wok
yield: 4 port
source: https://www.vegourmet.se/recept/mapo-tofu/
---

Total time: 1h 40 min (**not sure that's correct**)

## Ingredienser

- 4 port ris
- 3  vitlöksklyftor
- ¼  chilifrukt, mer för starkare rätt
- 3 msk färsk ingefära, hackad
- 400 g fast tofu, naturell
- 1,5 dl sötmandel, 100 g
- 3 msk tomatpuré
- 3 ½ dl vatten
- 1 tsk majsstärkelse, t ex Maizena
- ¾ tsk sichuanpeppar, stött
- ½ dl rapsolja
- 3 msk japansk sojasås
- ~2 tsk sesamolja
- 1 nypa socker
- 1 dl gräslök, finhackad

## Gör så här

Koka riset enligt anvisning på förpackningen.

Hacka vitlök, ingefära och urkärnad chilifrukt fint. Tärna tofun.

Mixa mandel, tomatpuré, vatten och majsstärkelse, enklast med stavmixer. Stöt
sichuanpeppar i en mortel.

Hetta upp oljan i en panna. Det ska vara ganska hett. Fräs vitlök, chili och
ingefära någon minut. Tillsätt sichuanpeppar mot slutet och därefter tofun.
Vänd runt försiktigt någon minut till.

Häll på mandelblandningen och låt koka ihop ett par minuter. Smaka av med
sojasås, sesamolja och en nypa socker och dekorera med finhackad gräslök.
