---
title: Classic Pico de Gallo
yield: 16 servings (1 quart)
categories: Condiments
source: https://www.seriouseats.com/classic-pico-de-gallo-salsa-fresca-recipe
---

Active: 10 mins
Total: 30 mins

## Ingredients

- 1 1/2 pounds (680g) ripe tomatoes, cut into 1/4- to 1/2-inch dice (about 3
  cups)*
- Kosher salt
- 1/2 large white onion, finely diced (about 3/4 cup)
- 1 to 2 serrano or jalapeño chilies, finely diced (seeds and membranes removed
  for a milder salsa)
- 1/2 cup finely chopped fresh cilantro leaves
- 1 tablespoon (15ml) lime juice from 1 lime

*: Use the ripest tomatoes you can find. In the off season, this generally
means smaller plum, Roma, or cherry tomatoes.

## Directions

Season tomatoes with 1 teaspoon (4g) salt and toss to combine. Transfer to a
fine-mesh strainer or colander set in a bowl and allow to drain for 20 to 30
minutes. Discard liquid.

Combine drained tomatoes with onion, chilies, cilantro, and lime juice. Toss to
combine and season to taste with salt. Pico de gallo can be stored for up to 3
days in a sealed container in the refrigerator.
