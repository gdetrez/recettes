---
title: "Instant Pot Cheatsheet"
---

## Cooking times

### Rice

| Rice              | Time  | NPR     |
| -------------     | ----- | ---     |
| White basmati[^4] | 3'    | ✓       |
| White sushi[^4]   | 5'    | ✓       |
| Brown basmati[^4] | 22'   | ✓       |
| Brown short[^4]   | 24'   | ✓       |
| red rice[^4]      | 30 '  | ✓       |
| wild blend[^4]    | 28'   | ✓       |
| wild rice[^4]     | 30'   | ✓       |
| black pearl[^4]   | 30'   | ✓       |
| brown rice[^3]    | 20'   | Unclear |


### Beans

| Bean            | Soakig | Time | NPR |
| -------------   | -----  | ---- | --- |
| Bondbönor[^2]   | -      | 1h10 | ✓   |
| Black Beans[^1] | -      | 30m  | ✓   |

## Yogurt temperatures

- “Normal” mode for making yogurt: 36 ~ 43° C (96.8 ~ 109.4°F);
- “Less” mode for making Jiu Niang (fermented glutinous rice): 30 ~ 34°C (86 ~ 93.2°F);
- “More” for pasteurizing milk: 71~83°C (160~180°F).

https://simmertoslimmer.com/instant-pot-idli/

[^1]: https://minimalistbaker.com/instant-pot-black-beans-perfect-beans-every-time/
[^2]: https://www.youtube.com/watch?v=z2QEhvTe78o
[^3]: https://minimalistbaker.com/instant-pot-brown-rice-perfect-every-time/
[^4]: https://greenhealthycooking.com/instant-pot-rice/
