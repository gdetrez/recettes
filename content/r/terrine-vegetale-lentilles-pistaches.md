---
title: "Terrine Vegetale Lentilles Pistaches"
date: 2023-11-19T20:46:49+01:00
categories: Condiments
source: https://www.notparisienne.fr/pour-lapero/terrine-vegetale-de-lentilles-aux-pistaches/
---

Pour une terrine de 300 g

## Ingrédients

- 175 g de tofu fumé
- 40 g de lentilles beluga ou brunes
- 35 g de pistaches
- 1 échalote
- 3 gousses d'ail
- 1 c. à s. de levure de bière
- 1 pincée de thym
- 1 c. à s. de moutarde à l'ancienne
- 4 cl de jus de cuisson des lentilles
- Sel, poivre

## Instructions

Faites cuire les lentilles pendant 30 minutes, départ eau froide avec les
gousses d'ail non épluchées.

Egouttez mais conservez l'eau de cuisson.

Récupérez les gousses, épluchez-les.

Emincez l'échalote.

Décortiquez les pistaches et enlevez les peaux en les frottant dans vos mains.

Mixez les lentilles avec le tofu, les gousses d'ail, la levure de bière, le
thym, la moutarde, salez et poivrez si besoin.

Ajoutez du jus de cuisson pour obtenir un mélange homogène, il m'en a fallu 4
cl.

Ajoutez l'echalote, mixez rapidement pour mélanger sans trop mixer.

Ajoutez les pistaches et mélangez manuellement.

Versez dans un pot, tassez bien.

Gardez au frais jusqu'au service et accompagnez de cornichons et de bon pain
frais.
