---
title: "Beluga bolognese"
date: 2022-05-03
categories: Pasta
source: https://www.underpressure.se/2019/02/17/beluga-bolognese/
---

Makes: 4-6 portions

## Ingredienser

- 1 gul lök
- 3 vitlöksklyftor
- 1 tsk lökpulver
- 1 tsk paprikapulver
- 1 tsk chiliflingor
- 1 tsk rosmarin
- 2 lagerblad
- 2 msk miso
- 1 sötpotatis fintärnad
- 2 msk hackad persilja (frusen)
- 1 dl rödvin
- 1 burk krossade tomater
- 1 umami buljongtärning
- 1,5 dl vatten
- 1,5 dl beluga linser
- 100 gr creme cheese

## Instruktioner

1. Hacka lök och vitlök. Starta din tryckkokares Sauté-funktion under tiden.
2. Fräs löken några minuter i olja eller smör så att den mjuknar och blir genomskinlig.
3. Tillsätt de torra kryddorna och låt dem hettas upp och fräsa med en stund så
   smakerna frigörs. Tryck sedan på Stop så sauteringen avslutas.
4. Skala sötpotatisen och tärna den i små bitar ca:1x1cm.
5. Lägg ned alla ingredienser förutom färskosten.
6. Sätt på locket och stäng ventilen. Tryck Manual och ställ tiden på 15 minuter.
7. När tiden är ute, låt trycket sjunka under tio minuter innan du öppnar
   ventilen och släpper ut det sista av trycket.
8. Plocka upp lagerbladen och rör om lite och om sötpotatisen inte riktigt
   kokat sönder, kan du pressa den lite lätt med en slev. Rör sedan ned
   färskosten.
9. Smaka av med salt och peppar.
10. Servera med rikliga mängder av parmesanost!
