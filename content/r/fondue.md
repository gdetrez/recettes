---
title: Fondue
categories: Divers
yield: 4
source: Livre de cuisine Hazebrouck
---

Prép. et cuisson: 15 mn
Serves: 4

## Il faut

- Fromage de gruyère, 600 à 800 g.
- Ail, 2 gousses
- Vin blanc sec, 3 verres.
- Kirsch, 2 verres à liqueur.
- Fécule, 1 cuirrerée à café.
- Bicarbonate de soude, 2 pincées
- Muscade râpée, 2 pincées.
- Pain, quelques tranches.
- Sel, poivre.

## Préparation

1. Coupez le pain en petits morceaux. Mettez-en dans chaque assiette.
2. Râpez le fromage en copeaux. Frottez d'ail un poêlon. Laissez-y le reste des
   gousses. Versez-y le vin. Faites chauffer sur feu moyen. Ajoutez-y peu à peu
   les copeaux de fromage, sans cesser de tourner aver une cuiller en bois.
3. Lorsque la fondue prend consistance, incorporez-y le kirsch, le bicarbonate,
   la fécule, la muscade râpée. Poivrez et salez si c'est nécessaire.
4. Apportez la fondue à table et maintenez-la au chaud sur un réchaud. Chaque
   convive fait glisser un morceau de pain entre les dents de sa fourchette et
   le trempe dans la fondue en opérant un mouvement tournant.

## Remarques

Autant que possible, faites la fondue avec deux sortes do fromages: moitié
gruyère, moitié emmenthal, par example, ou même du gruyère de deux meules
différentes.

Le bicarbonate mis en fin de cuisson rend la fondue plus digestible. La fécule
l'empêche de tourner, mais si l'accident se produisait malgré tout, un goutte
de vinaigre rattraperait la fondue.

Si vous tenez à servir quelque chose avant la fondue, que ce soit de simples
crudités ou un léger potage. Pour terminer, un entremets aux fruits ou, mieux
encore, une glace. Comme boisson : un vin blanc sec bien frais, suisse de
préférence (tel que fendant ou dezaley) ou un vin blanc d'Alsace.
