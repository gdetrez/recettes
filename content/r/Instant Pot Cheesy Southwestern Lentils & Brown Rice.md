---
title: "Instant Pot Cheesy Southwestern Lentils & Brown Rice"
date: 2022-06-06T15:39:45+02:00
categories: Onepot
tags: ["Instant Pot"]
source: https://www.platingsandpairings.com/instant-pot-cheesy-southwestern-lentils-brown-rice/
---

# Ingredients

- 1 small red onion (finely chopped)
- ¼ red bell pepper (finely chopped)
- 2 garlic cloves (minced)
- 1 dl brown rice
- 1 dl brown lentils
- 3 dl vegetable broth
- ½ can tomatoes
- ½ 4-ounce can diced green chiles
- ½ tbsp taco seasoning (mexican mix)
- 1 tsp dried oregano
- ½ tsp kosher salt
- ¼ tsp Black pepper
- 1 cups shredded cheese (I prefer mozzarella and sharp cheddar)
- 2 tbsp chopped fresh cilantro (for topping)

# Instructions

Add all ingredients, except cheese and cilantro, to your Instant Pot. Set to
manual and cook on high pressure for 15 minutes. Allow pressure to naturally
release for 15 minutes then release remaining pressure. 

Remove cover and stir in half of the cheese. Sprinkle remaining cheese over the
top and replace the cover. Allow to stand for 5 minutes.

Sprinkle with cilantro and serve. Enjoy! 
