---
title: "Okonomiaki-ish"
date: 2023-09-18T19:39:02+02:00
categories: Crêpes
---

| Serves | 2 okonomiyaki (2-3 pers) |

## Ingrédients

Batter:

- 2 dl levain
- 250g de choux
- 100-150g autre légumes rapés ou émincés
  (oignon, carrottes, bettraves, algues, champignons…)
- 2 oeufs
- sel, poivre

Toppings:

- Okonomiyaki sauce
- kewpie mayo
- Shichimi Togarashi
- Nori
- …
