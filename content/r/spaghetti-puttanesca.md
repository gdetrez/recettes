---
title: "Spaghetti Puttanesca"
date: 2023-10-22T18:10:47+02:00
yield: 4 personer
categories: Pasta
source: https://www.seriouseats.com/spaghetti-puttanesca-pasta-week-capers-olives-anchovies-recipe
---

## Ingredients

- 6 tablespoons (90ml) extra-virgin olive oil, divided
- 4 medium garlic cloves, thinly sliced or finely chopped by hand (see notes)
- 4 to 6 anchovy fillets, finely chopped (1 1/2 to 2 tablespoons)
- Large pinch red pepper flakes
- 1/4 cup capers, drained and chopped (about 2 ounces; 60g) (see notes)
- 1/4 cup chopped pitted black olives (about 2 ounces; 60g) (see notes)
- 1 cup (225g) whole peeled tomatoes, preferably San Marzano, roughly broken up by hand (about half a 14-ounce can)
- One 5-ounce (140g) can oil-packed tuna (optional)
- Kosher salt
- 8 ounces (225g) dried spaghetti
- Small handful minced fresh parsley leaves
- 1 ounce (30g) finely grated Pecorino Romano or Parmesan cheese, plus more for serving
- 1/4 teaspoon freshly ground black pepper

## Directions

In a medium skillet, combine 4 tablespoons (60ml) oil, garlic, anchovies, and
red pepper flakes. Cook over medium heat until garlic is very lightly golden,
about 5 minutes. (Adjust heat as necessary to keep it gently sizzling.) Add
capers and olives and stir to combine.

Add tomatoes, stir to combine, and bring to a bare simmer. If using, stir in
canned tuna, flaking it gently with a fork. Remove from heat.

Meanwhile, in a 12-inch skillet, 12-inch sauté pan, or large saucepan of
lightly salted boiling water, cook spaghetti until just shy of al dente, about
2 minutes less than package directions.

Using tongs, transfer pasta to sauce. Alternatively, drain pasta through a
colander, reserving 1 cup of the cooking water. Add drained pasta to sauce.

Add a few tablespoons of pasta water to sauce and set over medium-high heat to
bring pasta and sauce to a vigorous simmer. Cook, stirring and shaking the pan
and adding more pasta water as necessary to keep sauce loose, until pasta is
perfectly al dente, 1 to 2 minutes longer. (The pasta will cook more slowly in
the sauce than it did in the water.) Remove from heat and stir in remaining
olive oil, parsley, and cheese. Season with salt and pepper (be generous with
the pepper and scant with the salt—the dish will be plenty salty from the other
ingredients). Serve immediately with more grated cheese at the table.
