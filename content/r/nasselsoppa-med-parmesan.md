---
title: Nässelsoppa med parmesan
categories: Soupes
source: https://www.landleyskok.se/recept/nasselsoppa-med-parmesan
---


Serves: 6-8 personer som smårätt

## Notes

Modification de la recette originale: 3dl crème au lieu de 4dl. Essayer avec
2dl mais plus d'eau?

## Ingredients

Nässelsoppa:
- 4 msk smör
- 100g nässlor
- 1 gul lök
- 1 vitlöksklyfta
- 3 dl vispgrädde
- 1 dl vatten
- 1 msk äppelcidervinäger
- 1 krm chilipeppar
- salt, peppar

Topping:
- 2 dl riven parmesanost

## Directions

1. Repa bladen från nässlorna och se till att de är helt rena från bladlöss.

2. Hetta upp smör i en stekpanna och ha ner nässlorna. Låt dem fräsa 1-2
   minuter.

3. Hacka löken och låt den mjukna i smör i en kastrull. När löken mjuknat, ha
   ner finhackad vitlök. Ös över nässlorna och låt allt fräsa ihop.

4. Slå på grädde och vatten. Låt allt koka ihop i cirka 8 minuter. Kör allt
   till en slät soppa med hjälp av en mixerstav. Ställ tillbaka på plattan och
   smaka av med äppelcidervinäger, chilipulver, salt och peppar. Servera i små
   koppar eller glas med riven parmesanost på toppen.
